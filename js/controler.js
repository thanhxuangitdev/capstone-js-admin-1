// LOADING
export let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
export let offLoading = () => {
  document.getElementById("loading").style.display = "none";
};
//RENDER
export let renderProducts = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    contentHTML += `<tr>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td>${item.screen}</td>
    <td>${item.backCamera}</td>
    <td>${item.frontCamera}</td>
    <td><img src="${item.img}" alt="chưa có hình ảnh" width="20%"></td>
    <td>${item.desc}</td>
    <td>${item.type}</td>
    <td>
    <button data-toggle="modal" data-target="#myModal" class="btn btn-secondary" onclick="getDetailProducts(${item.id})" >Edit</button>
    <button onclick="deleteProducts(${item.id})" class="btn btn-danger">Delete</button>
    </td>
    </tr>`;
  });
  document.getElementById("tbody").innerHTML = contentHTML;
};
// GET VALUE IN FROM
export let getValueInput = () => {
  const nameValue = document.getElementById("name").value;
  const priceValue = document.getElementById("price").value;
  const screenValue = document.getElementById("screen").value;
  const backCameraValue = document.getElementById("backCamera").value;
  const frontCameraValue = document.getElementById("frontCamera").value;
  const imgValue = document.getElementById("img").value;
  const descValue = document.getElementById("desc").value;
  const typeValue = document.getElementById("type").value;
  return {
    name: nameValue,
    price: priceValue,
    screen: screenValue,
    backCamera: backCameraValue,
    frontCamera: frontCameraValue,
    img: imgValue,
    desc: descValue,
    type: typeValue,
  };
};
// SHOW INFORMATION UP FORM
export let showInformation = (item) => {
  document.getElementById("name").value = item.name;
  document.getElementById("price").value = item.price;
  document.getElementById("screen").value = item.screen;
  document.getElementById("backCamera").value = item.backCamera;
  document.getElementById("frontCamera").value = item.frontCamera;
  document.getElementById("img").value = item.img;
  document.getElementById("desc").value = item.desc;
  document.getElementById("type").value = item.type;
};

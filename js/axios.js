//AXIOS
const BASE_URL = 'https://633ec04783f50e9ba3b75fed.mockapi.io';

export let getListProducts = () => {
    return axios({
        url: `${BASE_URL}/capstoneJS`,
        method: 'GET',
    });
};

export let getProductsId = (id) => {
    return axios({
        url: `${BASE_URL}/capstoneJS/${id}`,
        method: 'GET',
    });
};

export let postCreateProducts = (element) => {
    return axios({
        url: `${BASE_URL}/capstoneJS`,
        method: 'POST',
        data: element,
    });
};

export let deleteProductsByIdAxios = (id) => {
    return axios({
        url: `${BASE_URL}/capstoneJS/${id}`,
        method: 'DELETE',
    });
};

export let putProductsId = (id, element) => {
    return axios({
        url: `${BASE_URL}/capstoneJS/${id}`,
        method: 'PUT',
        data: element,
    });
};
